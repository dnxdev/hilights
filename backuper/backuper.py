import os
import sys
from datetime import datetime, timedelta

from dropbox import Dropbox
from dropbox.files import WriteMode
from dropbox.exceptions import ApiError, AuthError


PROJECT_NAME = os.getenv('PROJECT_NAME')
TOKEN = os.getenv('DROPBOX_TOKEN')

LOCAL_FILE = '/backuper/db.7z'
BACKUP_PATH = '/{}/{}.db.backup_{}'.format(PROJECT_NAME, PROJECT_NAME, datetime.now().strftime('%d-%m-%Y'))


def backup():
    with open(LOCAL_FILE, 'rb') as f:
        print("Uploading {} to Dropbox as {} ...".format(LOCAL_FILE, LOCAL_FILE))

        try:
            dbx.files_upload(f.read(), BACKUP_PATH, mode=WriteMode('overwrite'))
        except ApiError as err:
            # This checks for the specific error where a user doesn't have
            # enough Dropbox space quota to upload this file
            if err.error.is_path() and err.error.get_path().reason.is_insufficient_space():
                sys.exit("ERROR: Cannot back up; insufficient space.")
            elif err.user_message_text:
                print(err.user_message_text)
                sys.exit()
            else:
                print(err)
                sys.exit()


def remove():
    old_date = (datetime.now() - timedelta(days=7)).strftime('%d-%m-%Y')
    old_file_path = '/{}/{}.db.backup_'.format(
        PROJECT_NAME, PROJECT_NAME, old_date
    )
    try:
        dbx.files_delete(old_file_path)
    except ApiError as err:
        if err.error.is_path_lookup() and err.error.get_path_lookup().not_found.is_not_found():
            print("Old backup file from {} not found".format(old_date))
        elif err.user_message_text:
            print(err.user_message_text)
        else:
            print(err)


if __name__ == '__main__':
    # Create an instance of a Dropbox class, which can make requests to the API.
    print("Creating a Dropbox object...")
    dbx = Dropbox(TOKEN)

    # Check that the access token is valid
    try:
        dbx.users_get_current_account()
    except AuthError as err:
        sys.exit("ERROR: Invalid access token; try re-generating an access token from the app console on the web.")

    # Create a backup of the current settings file
    backup()
    remove()

    print("Done!")
