var gulp          = require('gulp'),
	sass          = require('gulp-sass'),
	concat        = require('gulp-concat'),
	cleancss      = require('gulp-clean-css'),
	rename        = require('gulp-rename'),
	autoprefixer  = require('gulp-autoprefixer'),
	browsersync	  = require('browser-sync');
	notify        = require("gulp-notify");


gulp.task('browser-sync', function() {
	browsersync({
		proxy: "server:8000",
		notify: false,
		open: false,
	})
});


gulp.task('styles', function() {
	return gulp.src('source/sass/**/*.sass')
		.pipe(sass({ outputStyle: 'expand' }).on("error", notify.onError()))
		.pipe(rename({ suffix: '.min', prefix : '' }))
		.pipe(autoprefixer(['last 15 versions']))
		.pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
		.pipe(gulp.dest('build/css'))
});


gulp.task('js', function() {
	return gulp.src([
			'source/libs/jquery/dist/jquery.min.js',
			'source/libs/bootstrap/bootstrap.min.js',
			'source/libs/fullpage/scrolloverflow.min.js',
			'source/libs/fullpage/jquery.fullpage.min.js',
			'source/libs/owlCarousel/owl.carousel.min.js',
			'source/libs/bootstrap/bootstrap-slider.min.js',
			'source/libs/fancybox/jquery.fancybox.min.js',
			'source/libs/mixitup.min.js',
			'source/libs/jquery.scroll-with-ease.js',
			'source/libs/jquery.maskedinput.min.js',
			'source/js/**/*.js'
		])
		.pipe(concat('scripts.min.js'))
		.pipe(gulp.dest('build/js'))
});


gulp.task('fonts', function() {
	return gulp.src('source/fonts/**/*.{eot,ttf,woff,woff2}')
		.pipe(gulp.dest('build/fonts/'))
});


gulp.task('images', function() {
	return gulp.src('source/img/**/*.{jpg,jpeg,png,ico}')
		.pipe(gulp.dest('build/img/'))
});


gulp.task('watch', ['styles', 'js', 'browser-sync'], function() {
	gulp.watch('source/sass/**/*.sass', ['styles']);
	gulp.watch('source/js/**/*.js', ['js']);
	gulp.watch('source/fonts/**/*.{eot,ttf,woff,woff2}', ['fonts']);
	gulp.watch('source/img/**/*.{jpg,jpeg,png,ico}', ['images']);
	gulp.watch('source/templates/**/*.html', browsersync.reload)
});


gulp.task('build', ['styles', 'js', 'fonts', 'images']);
gulp.task('dev', ['build', 'watch']);
gulp.task('default', ['watch']);
