$(function() {

	$('.site-header__menu-info-burger, #menu a').click(function() {
		$('.site-header-burger-content').toggle('slow');
	});
	$('.burger-close').click(function() {
		$('.site-header-burger-content').toggle('slow');
	});

	$(document).ready(function() {
		$('#fullpage').fullpage({
			autoScrolling: false,
			fitToSection: false,
			menu: '#menu',
			scrollBar: false,
			anchors:['main-block', 'offer', 'about', 'showreel', 'case', 'help', 'group', 'team', 'about1', 'footer'],
		});
	});

	$("#inputPhone").mask("+7(999)999-9999");

	var height = $(".site-header__block-title li").height();
	$(".site-header__block-title ul").height(height);
	$( window ).resize(function() {
		var height = $(".site-header__block-title li").height();
		$(".site-header__block-title ul").height(height);
	});

	$(document).ready(function(){
		$(".group__block").owlCarousel({
			loop: true,
			autoplay: true,
			autoplayTimeout: 2500,
			slideBy: 3,
			autoplayHoverPause: true,
			responsive: {
				768: {
					items: 6,
				},
				0: {
					items: 3,
				}
			}
		});
	});

	var $hamburger = $(".hamburger");
	$hamburger.on("click", function(e) {
		$hamburger.toggleClass("is-active");
		// Do something else, like open/close menu
	});

	$('ul#menu li').on("click", function(e) {
		$(".hamburger").toggleClass("is-active");
		// Do something else, like open/close menu
	});

	$(document).ready(function($){
		$('.site-footer').mousemove(
			function(e){
				/* Work out mouse position */
				var offset = $(this).offset();
				var xPos = e.pageX - offset.left;

				/* Get percentage positions */
				var mouseXPercent = Math.round(xPos / $(this).width() * 100);

				/* Position Each Layer */
				$(this).children('img').each(
					function(){
						var diffX = $('.site-footer').width() - $(this).width();

						var myX = diffX * (mouseXPercent / 100); //) / 100) / 2;


						var cssObj = {
							'left': myX + 'px',
						}
						//$(this).css(cssObj);
						$(this).animate({left: myX},{duration: 0, queue: false, easing: 'linear'});

					}
				);

			}
		);
	});

	$('#feedback_phone_form').click (function(e) {
		e.preventDefault();
		$('.feedback-form, .feedback-form__placeholder').toggle();
		$('.feedback-form').css('display', 'flex');
	});

	$(".feedback-form__block-close").click(function(e) {
		e.preventDefault();
		$('.feedback-form, .feedback-form__placeholder').toggle();
		$('.feedback-form').css('display', 'none');
	});

	$(".feedback-form-sucesses__block-close").click(function(e) {
		e.preventDefault();
		$('.feedback-form-sucesses, .feedback-form__placeholder').toggle();
		$('.feedback-form-sucesses').css('display', 'none');
	});

	$(".feedback-form-sucesses__block-submit").click(function(e) {
		e.preventDefault();
		$('.feedback-form-sucesses, .feedback-form__placeholder').toggle();
		$('.feedback-form-sucesses').css('display', 'none');
	});

	$("#feedback_form").submit( function(event) {
		event.preventDefault(); //запрет на отправку формы браузеру

		$('.feedback-form').css('display', 'none');
		$('.feedback-form-sucesses').css('display', 'flex');

		var form = $(this);

		$.ajax({
			type: form.attr('method'),
			url: form.attr('action'),
			data: form.serialize()
		});
	});

	$(window).on('hashchange',function(){
		if ( $(window).width() > 768 && $("#video-background").length ) {
			if (location.hash == '#main-block' ) {
				$("#video-background").get(0).play();
			} else {
				$("#video-background").get(0).pause();
			}
		}
    });

	$('.play').mouseover(function(){
		if ( $(window).width() > 768 && $("#video-showreel").length ) {
            $("#video-showreel").get(0).play();
        }
	});

	$('.play').mouseleave(function(){
		if ( $(window).width() > 768 && $("#video-showreel").length ) {
	 		$("#video-showreel").get(0).pause();
		}
	});

	if ($('#showreel').length) {
        if ($(window).width() < 768) {
            $('.site-header').css({
                'background': 'url("static/landing/img/main-bg.png") no-repeat center center',
                'background-size': 'cover'
            });
            $('.showreel').css({
                'background': 'url("static/landing/img/showreel-bg.jpg") no-repeat center center',
                'background-size': 'cover'
            })
        } else {
            var source_background = document.querySelector('#video-background source'),
                source_showreel = document.querySelector('#video-showreel source');

            if (source_background != null) {
                source_background.setAttribute('src', source_background.getAttribute('video-src'));
            }

            if (source_showreel != null) {
                source_showreel.setAttribute('src', source_showreel.getAttribute('video-src'));
            }

        }
	}

	if ( $('#work-full').length ) {
        $(window).on('scroll', function () {
            $('header').css({
                opacity: 1 - $(window).scrollTop() * 0.0030
            })

            $('.site-header__block-title').css({
                top: $(window).scrollTop() * 0.65
            })
        });

        $(document).ready(function () { //анимация для блоков с видео
            var containerEl = document.querySelector('.tab-block-list');

            var mixer = mixitup(containerEl, {
                selectors: {
                    target: '.tab-block-item'
                },
                load: {
                    filter: 'none',
                }
            });

            containerEl.classList.add('mixitup-ready');
            mixer.show();
        });

        $(document).ready(function () {
            $('html').scrollWithEase();
        });
    }
});