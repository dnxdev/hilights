$(".span2").slider({})

$('.form-field').click(function () {
	$(this).find('input:radio').prop('checked', true);
});

$("#feedback_phone").mask("+7(999)999-9999");

var currentTab = 0; // Current tab is set to be the first tab (0)
var currentSale = 0;

if ($('#questions').length) {
	showTab(currentTab); // Display the current tab
}

function showTab(n) {
	var all_questions = document.getElementsByClassName("tab");
	all_questions[n].style.display = "block";

	if (n == 0) {
		document.getElementById("progress_bar").innerHTML = 'Ваша текущая скидка: 0%';
		document.getElementById("prevBtn").style.display = "none";
	} else {
		document.getElementById("prevBtn").style.display = "inline";
	}

	if (n == (all_questions.length - 1)) {
		document.getElementById("progress_bar").innerHTML = 'Процент выполнения: 100%';
		document.getElementById("progress_bar").style.marginBottom = '8px';
		document.getElementById("title").style.display = "none";

		document.getElementById("question").style.textAlign = "center";
		document.getElementById("question").innerHTML = "Остался последний шаг!";

		document.getElementById("final_sale").innerHTML = currentSale.toString() + ' %';
		
		document.getElementById("nextBtn").innerHTML = "Заказать звонок";
	} else {
		document.getElementById("progress_bar").style.marginBottom = '30px';
		document.getElementById("progress_bar").innerHTML = 'Ваша текущая скидка: ' + currentSale.toString() + ' %';
		document.getElementById("title").style.display = "block";

		document.getElementById("question").removeAttribute("style");
		var input_question = all_questions[n].getElementsByTagName("input");
		document.getElementById("question").innerHTML = input_question[0].name;

		document.getElementById("nextBtn").innerHTML = "Следующий шаг";
	}
}

function nextPrev(n) {
	var all_questions = document.getElementsByClassName("tab");

	if (n == 1 && !validateForm()) {
		return false;
	}

	if (currentTab+n >= all_questions.length) {
		// send form on server
		var form = $('#quiz');
		$.post({
			type: form.attr('method'),
			url: form.attr('action'),
			data: form.serialize()
		});

		close_question_field();

		return false;
	} else {
		all_questions[currentTab].style.display = "none";
		currentTab = currentTab + n;
		currentSale = currentSale + n * 5;
		var progress = ((currentTab/(all_questions.length-1) * 100) | 0).toString();
		document.getElementById("progress_bar").innerHTML = 'Ваша текущая скидка: ' + currentSale.toString() + ' %';
		$(".progress-bar").css("width", "" + progress + "%");
		showTab(currentTab);
	}
}

function validateForm() {
	var all_questions = document.getElementsByClassName("tab");
	var input_question = all_questions[currentTab].getElementsByTagName("input");

	for (var i = 0; i < input_question.length; i++) {
		if (input_question[i].type == 'radio') {
			if (input_question[i].checked == true){
				return true;
			}
		} else if (input_question[i].value == "") {
			return false;
		} 
	}

	if (input_question[0].type == 'radio') {
		return false;
	} else {
		return true;
	}
	
}

$(".toggle-question, .questions__close, #quiz_form").click(function(e) {
	e.preventDefault();
	close_question_field();
});

function close_question_field() {
	$(".questions").fadeToggle("fast", "linear");
	$(".feedback-form__placeholder").fadeToggle("fast", "linear");
	$(".questions").css("display", "flex");
}
