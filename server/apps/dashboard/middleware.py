from django.conf import settings
from django.utils.deprecation import MiddlewareMixin


class DashboardMiddleware(MiddlewareMixin):
    def process_template_response(self, request, response):
        if request.path == '/admin/':
            response.context_data['app_list'] = settings.DASHBOARD
        return response
