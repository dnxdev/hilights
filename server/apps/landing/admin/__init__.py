from django.contrib import admin
from solo.admin import SingletonModelAdmin

from apps.landing.models import *
from .quiz import QuestionAdmin
from .video_case import VideoCaseAdmin


admin.site.register(Clients)
admin.site.register(Team)
admin.site.register(TypeVideo)
admin.site.register(VideoPortfolio)

admin.site.register(Showreel, SingletonModelAdmin)
admin.site.register(BackgroundVideo, SingletonModelAdmin)
admin.site.register(Offer, SingletonModelAdmin)

admin.site.register(Question, QuestionAdmin)
admin.site.register(VideoCase, VideoCaseAdmin)