from django.contrib.admin import ModelAdmin, StackedInline
from adminsortable2.admin import SortableAdminMixin

from apps.landing.models import InputAnswer, ChoiceAnswer, SliderAnswer, Question


class InputAnswerInline(StackedInline):
	model = InputAnswer
	extra = 0
	max_num = 1


class ChoiceAnswerInline(StackedInline):
	model = ChoiceAnswer
	extra = 0
	max_num = 8	


class SliderAnswerInline(StackedInline):
	model = SliderAnswer
	extra = 0
	max_num = 1


class QuestionAdmin(SortableAdminMixin, ModelAdmin):
	model = Question
	inlines = [InputAnswerInline, ChoiceAnswerInline, SliderAnswerInline]




