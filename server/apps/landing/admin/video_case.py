from django.contrib.admin import ModelAdmin
from adminsortable2.admin import SortableAdminMixin

from apps.landing.models import VideoCase


class VideoCaseAdmin(SortableAdminMixin, ModelAdmin):
	model = VideoCase

	def has_add_permission(self, request):
		if VideoCase.objects.only('pk').count() == 6:
			return False
		else:
			return True