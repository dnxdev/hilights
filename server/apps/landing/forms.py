from django import forms


class CallbackForm(forms.Form):
	name = forms.CharField(max_length=20)
	phone = forms.CharField(max_length=20)
