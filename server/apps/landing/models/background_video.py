from django.db.models import FileField
from solo.models import SingletonModel

from django.core.validators import FileExtensionValidator


class BackgroundVideo(SingletonModel):
	video = FileField(
		verbose_name="Фоновое видео",
		upload_to='background_video/',
		validators=[FileExtensionValidator(allowed_extensions=['mp4'])]
	)

	class Meta:
		verbose_name = 'Фоновое видео'
		verbose_name_plural = 'Фоновое видео'