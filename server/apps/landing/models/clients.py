from django.db import models

from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFit


class Clients(models.Model):
	name = models.CharField(
        max_length=255,
        verbose_name='Название компании'
    )
	picture = ProcessedImageField(
        verbose_name='Изображение',
        upload_to='clients/',
        processors=[
            ResizeToFit(
                width=100,
            )
        ],
    )

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Логотип клиента'
		verbose_name_plural = 'Логотипы клиентов'