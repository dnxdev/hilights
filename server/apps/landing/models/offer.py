from django.db import models
from solo.models import SingletonModel


class Offer(SingletonModel):
    title = models.CharField(
        max_length=255,
        verbose_name='Заголовок'
    )
    offer = models.TextField(
        verbose_name='Предложение'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Предложение'
        verbose_name_plural = 'Предложения'