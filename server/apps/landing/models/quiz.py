from django.db import models


class Question(models.Model):
    question = models.CharField(
        max_length=255,
        verbose_name='Вопрос'
    )
    order = models.PositiveIntegerField(
        default=0
    )

    def __str__(self):
        return self.question

    class Meta:
        ordering = ('order',)
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'


class InputAnswer(models.Model):
    description_input = models.CharField(
        max_length=30,
        verbose_name='Подсказка в поле ввода ответа'
    )
    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE,
        related_name='input_answer'
    )


class ChoiceAnswer(models.Model):
    choice_text = models.CharField(
        max_length=22,
        verbose_name='Ответ для выбора'
    )
    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE,
        related_name='choice_answer'
    )


class SliderAnswer(models.Model):
    description_slider = models.CharField(
        max_length=255,
        verbose_name='Подсказка к ползунку выбора диапазона'
    )
    min_value = models.PositiveIntegerField(
        verbose_name='Минимальное значение ползунка'
    )
    max_value = models.PositiveIntegerField(
        verbose_name='Максимальное значение ползунка'
    )
    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE,
        related_name='slider_answer'
    )