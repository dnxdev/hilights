from django.db.models import FileField
from solo.models import SingletonModel

from django.core.validators import FileExtensionValidator


class Showreel(SingletonModel):
	video = FileField(
		verbose_name="Showreel",
		upload_to='showreel/',
		validators=[FileExtensionValidator(allowed_extensions=['mp4'])]
	)

	def __str__(self):
		return 'Showreel'

	class Meta:
		verbose_name = 'Showreel'
		verbose_name_plural = 'Showreel'