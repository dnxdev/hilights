from django.db.models.signals import pre_delete, pre_save
from django.dispatch import receiver

from apps.landing.models import Clients, Team, Showreel, BackgroundVideo, VideoCase, VideoPortfolio


@receiver((pre_save, pre_delete), sender=Clients)
@receiver((pre_save, pre_delete), sender=Team)
@receiver((pre_save, pre_delete), sender=VideoCase)
@receiver((pre_save, pre_delete), sender=VideoPortfolio)
@receiver((pre_save, pre_delete), sender=Showreel)
@receiver((pre_save, pre_delete), sender=BackgroundVideo)
def delete_file(sender, instance, **kwargs):
    obj = instance.__class__.objects.filter(pk=instance.pk).first()

    if not obj:
        return

    file_fields_name = [field.name for field in instance._meta.get_fields() if field.get_internal_type() == 'FileField']

    if kwargs.get('raw', None) is not None: # check pre_save signal
        file_fields_name = [field for field in file_fields_name if getattr(instance, field) != getattr(obj, field)]

    for field in file_fields_name:
        try:
            storage, path = getattr(obj, field).storage, getattr(obj, field).path
            storage.delete(path)
        except:
            continue
