from django.db import models

from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill


class Team(models.Model):
	name = models.CharField(
        max_length=255,
        verbose_name='Имя'
    )
	profile = models.TextField(
        verbose_name='Краткое описание нарпавления подготовки'
    )
	picture = ProcessedImageField(
        verbose_name='Аватар',
        upload_to='team/',
        processors=[ResizeToFill(480,640)]
    )

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Член команды'
		verbose_name_plural = 'Члены команды'