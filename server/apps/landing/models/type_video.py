from django.db import models


class TypeVideo(models.Model):
	name = models.CharField(
		max_length=255,
		verbose_name='Тип видео'
	)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Тип видео'
		verbose_name_plural = 'Типы видео'
