from django.db import models

from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill


class VideoCase(models.Model):
    title = models.CharField(
        max_length=255,
        verbose_name='Заголовок'
    )
    description = models.TextField(
        verbose_name='Описание'
    )
    picture = ProcessedImageField(
        verbose_name='Превью видео',
        upload_to='video_case/',
        processors=[ResizeToFill(800, 480)]
    )
    link_to_video = models.CharField(
        max_length=255,
        verbose_name='Ссылка на видео'
    )
    order = models.PositiveIntegerField(
        default=0
    )

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order',)
        verbose_name = 'Видео для кейса'
        verbose_name_plural = 'Видео для кейсов'