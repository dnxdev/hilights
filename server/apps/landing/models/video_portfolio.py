from django.db import models

from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill


class VideoPortfolio(models.Model):
    title = models.CharField(
        max_length=255,
        verbose_name='Заголовок'
    )
    description = models.TextField(
        verbose_name='Описание'
    )
    picture = ProcessedImageField(
        verbose_name='превью',
        upload_to='video_portfolio/',
        processors=[ResizeToFill(800, 480)]
    )
    link_to_video = models.CharField(
        max_length=255,
        verbose_name='Ссылка на видео'
    )
    type_video = models.ManyToManyField(
        'landing.TypeVideo',
        verbose_name='Катигории видео'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Видео для портфолио'
        verbose_name_plural = 'Видео для портфолио'
