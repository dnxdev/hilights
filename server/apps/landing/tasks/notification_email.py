from django.core.mail import EmailMessage
from des.models import DynamicEmailConfiguration

from config.celery import app
from constance import config


@app.task
def notification_email(title, text):
    email_config = DynamicEmailConfiguration.get_solo()

    msg = EmailMessage(
        subject=title,
        body=text,
        from_email=email_config.from_email,
        to=[config.EMAIL_NOTIFICATION]
    )

    msg.send()
