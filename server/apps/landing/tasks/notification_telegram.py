import socket
import socks
import urllib

from constance import config
from config.celery import app


@app.task
def notification_telegram(text):
    default_socket = socket.socket

    socks.setdefaultproxy(socks.SOCKS5, 'tor', 9050)
    socket.socket = socks.socksocket

    urllib.request.urlopen(
        config.TELEGRAM_BASE_URL +
        config.TELEGRAM_TOKEN_BOT +
        '/sendMessage?' +
        urllib.parse.urlencode({
            'chat_id': config.TELEGRAM_CHAT_ID,
            'text': text,
            'parse_mode': 'Markdown'
        })
    )

    socket.socket = default_socket
