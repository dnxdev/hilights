from django.urls import path
from apps.landing.views import IndexView, PortfolioView, QuizView, CallbackView


urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('portfolio/', PortfolioView.as_view(), name='portfolio'),

    path('quiz/', QuizView.as_view(), name='quiz'),
    path('call/', CallbackView.as_view(), name='call'),
]