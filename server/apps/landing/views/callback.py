from django.views import View
from django.http import HttpResponse

from apps.landing.forms import CallbackForm
from apps.landing.tasks import notification_telegram, notification_email


class CallbackView(View):
    def post(self, request, *args, **kwargs):
        form = CallbackForm(request.POST)

        if form.is_valid():
            text = 'Имя: {}\nТелефон: {}'.format(form.cleaned_data['name'], form.cleaned_data['phone'])

            notification_email.delay('Возможный клиент!', text)
            notification_telegram.delay('*Возможный клиент!*\n{}'.format(text))

        return HttpResponse(None)

