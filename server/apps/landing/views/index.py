from django.views import View
from django.shortcuts import render

from constance import config

from apps.landing.models import *


class IndexView(View):
    def get(self, request, *args, **kwargs):
        data = {
            'questions': Question.objects.prefetch_related('input_answer', 'choice_answer', 'slider_answer'),
            'background': BackgroundVideo.get_solo(),
            'offer': Offer.get_solo(),
            'showreel': Showreel.get_solo(),
            'videocase': VideoCase.objects.all(),
            'clients': Clients.objects.all(),
            'team': Team.objects.all(),
            'phone': config.MY_PHONE,
            'email': config.MY_EMAIL,
            'vk': config.MY_GROUP_VK,
            'instagram': config.MY_GROUP_INSTAGRAM,
        }

        return render(request, 'landing/index.html', data)

