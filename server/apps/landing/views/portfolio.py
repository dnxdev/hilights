from django.views import View
from django.shortcuts import render

from constance import config

from apps.landing.models import TypeVideo, VideoPortfolio


class PortfolioView(View):
    def get(self, request, *args, **kwargs):
        data = {
            'typevideo': TypeVideo.objects.all(),
            'video': VideoPortfolio.objects.prefetch_related('type_video'),
            'phone': config.MY_PHONE,
            'email': config.MY_EMAIL,
            'vk': config.MY_GROUP_VK,
            'instagram': config.MY_GROUP_INSTAGRAM,
        }

        return render(request, 'landing/works.html', data)

