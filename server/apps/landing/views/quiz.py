from django.views import View
from django.http import HttpResponse

from apps.landing.tasks import notification_telegram, notification_email


class QuizView(View):
    def post(self, request, *args, **kwargs):
        text = ''

        for field in request.POST:
            if field not in ['csrfmiddlewaretoken', 'Телефон', 'Имя']:
                text += '{}: {}\n'.format(field, request.POST.get(field))

        text += 'Имя: {}\nТелефон: {}'.format(request.POST.get(u'Имя'), request.POST.get(u'Телефон'))

        notification_email.delay('Возможный клиент!', text)
        notification_telegram.delay('*Возможный клиент!*\n{}'.format(text))

        return HttpResponse(None)

