from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class MySitemap(Sitemap):
	priority = 1.0
	changefreq = 'weekly'

	def items(self):
		return ['index', 'portfolio']

	def location(self, item):
		return reverse(item)
