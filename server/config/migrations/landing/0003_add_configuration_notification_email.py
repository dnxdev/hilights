from django.db import migrations


def add_data(apps, schema_editor):
    DynamicEmailConfiguration = apps.get_model("des", "DynamicEmailConfiguration")

    DynamicEmailConfiguration.objects.create(
        host='smtp.yandex.ru',
        port=587,
        from_email='notification-hilights@yandex.ru',
        username='notification-hilights@yandex.ru',
        password='0BXLSLq8',
        use_tls=True
    )


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('landing', '0002_auto_20181104_1929'),
    ]

    operations = [
        migrations.RunPython(add_data),
    ]
