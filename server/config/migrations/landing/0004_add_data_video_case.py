from django.db import migrations


def add_data(apps, schema_editor):
    VideoCase = apps.get_model("landing", "VideoCase")

    VideoCase.objects.create(
        title='Аэросъемка',
        description='Лучшие кадры с высоты птичьего полёта.',
        picture='/video_case/3.png',
        link_to_video='https://www.youtube.com/watch?v=6pfupAERhxM',
        order=5
    )

    VideoCase.objects.create(
        title='Персональное видео',
        description='Видеоблоггинг.<br>Производство видеоклипов.<br>Организация персональных видеосессий.',
        picture='/video_case/2.png',
        link_to_video='https://www.youtube.com/watch?v=n_qs2awYAAk',
        order=4
    )

    VideoCase.objects.create(
        title='Бизнес инфографика',
        description='Создание анимированных<br>видеороликов для вашей компании.',
        picture='/video_case/5.png',
        link_to_video='https://www.youtube.com/watch?v=qQrfRyAuw_g',
        order=3
    )

    VideoCase.objects.create(
        title='Видео для бизнеса',
        description='Создание продающих и имиджевых<br>видеороликов для вашей компании.',
        picture='/video_case/1.png',
        link_to_video='https://www.youtube.com/watch?v=0XIaaF0O_gI',
        order=2
    )

    VideoCase.objects.create(
        title='Событийное видео',
        description='Памятное видео о вашем событии.<br>Видеосъемка корпоративных мероприятий, <br>выставок, форумов и других праздничных событий.',
        picture='/video_case/6.png',
        link_to_video='https://www.youtube.com/watch?v=fyXgvEHD0QE',
        order=1
    )

    VideoCase.objects.create(
        title='Рекламное видео',
        description='Реклама вашего продукта,<br>адаптированная под целевую аудиторию. <br>Разработка и реализация рекламных идей.',
        picture='/video_case/4.png',
        link_to_video='https://www.youtube.com/watch?v=YUgUzyqmxAc',
        order=0
    )


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('landing', '0003_add_configuration_notification_email'),
    ]

    operations = [
        migrations.RunPython(add_data),
    ]
