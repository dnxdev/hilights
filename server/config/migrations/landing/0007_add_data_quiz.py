from django.db import migrations


def add_data(apps, schema_editor):
    Question = apps.get_model("landing", "Question")
    InputAnswer = apps.get_model("landing", "InputAnswer")
    ChoiceAnswer = apps.get_model("landing", "ChoiceAnswer")
    SliderAnswer = apps.get_model("landing", "SliderAnswer")

    question = Question.objects.create(
        question='Назовите род вашей деятельности',
        order=0,
    )
    InputAnswer.objects.create(
        question=question,
        description_input='Введите ответ',
    )

    question = Question.objects.create(
        question='Сколько вам лет?',
        order=1,
    )
    InputAnswer.objects.create(
        question=question,
        description_input='Введите ответ',
    )

    question = Question.objects.create(
        question='Ваша целевая аудитория',
        order=2,
    )
    SliderAnswer.objects.create(
        question=question,
        min_value=18,
        max_value=60,
        description_slider='Выберите возрастной диапазон',
    )

    question = Question.objects.create(
        question='Какие жанры кино вы любите?',
        order=3,
    )
    ChoiceAnswer.objects.create(
        question=question,
        choice_text='Драма',
    )
    ChoiceAnswer.objects.create(
        question=question,
        choice_text='Комедия',
    )
    ChoiceAnswer.objects.create(
        question=question,
        choice_text='Документальный жанр',
    )
    ChoiceAnswer.objects.create(
        question=question,
        choice_text='Экшн',
    )

    question = Question.objects.create(
        question='Какую задачу должен решать видеоролик?',
        order=4,
    )
    ChoiceAnswer.objects.create(
        question=question,
        choice_text='Увеличение прибыли',
    )
    ChoiceAnswer.objects.create(
        question=question,
        choice_text='Информационное видео',
    )
    ChoiceAnswer.objects.create(
        question=question,
        choice_text='Видео на память',
    )
    ChoiceAnswer.objects.create(
        question=question,
        choice_text='Повышение имиджа',
    )
    ChoiceAnswer.objects.create(
        question=question,
        choice_text='Художественное видео',
    )
    ChoiceAnswer.objects.create(
        question=question,
        choice_text='Видеопрезентация',
    )


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('landing', '0006_auto_20181118_1933'),
    ]

    operations = [
        migrations.RunPython(add_data),
    ]
