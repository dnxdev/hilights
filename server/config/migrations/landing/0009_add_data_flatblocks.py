from django.db import migrations


def add_data(apps, schema_editor):
    FlatBlock = apps.get_model("flatblocks", "FlatBlock")

    FlatBlock.objects.create(
        slug='widgets',
        content='''
        <!-- WhatsHelp.io widget -->
        <script type="text/javascript">
            (function () {
                var options = {
                    whatsapp: "+79235867583", // WhatsApp number
                    telegram: "hilights_video", // Telegram bot username
                    vkontakte: "highlightvideo", // VKontakte page name
                    company_logo_url: "", // URL of company logo (png, jpg, gif)
                    greeting_message: "", // Text of greeting message
                    call_to_action: "", // Call to action
                    button_color: "#98c200", // Color of button
                    position: "right", // Position may be 'right' or 'left'
                    order: "whatsapp,telegram,vkontakte", // Order of buttons
                };
                var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
                var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
                s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
                var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
            })();
        </script>
        <!-- /WhatsHelp.io widget -->
    
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter49162438 = new Ya.Metrika2({
                            id:49162438,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                        });
                    } catch(e) { }
                });
    
                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/tag.js";
    
                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks2");
        </script>
        <!-- /Yandex.Metrika counter -->
        '''
    )


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('landing', '0008_auto_20181201_1700'),
    ]

    operations = [
        migrations.RunPython(add_data),
    ]
