from .installed_apps import *
from .settings import *
from .locale import *
from .celery import *
from .constance import *
from .email import *
from .dashboard import *
