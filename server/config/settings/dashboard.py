DASHBOARD = [
     {'name': 'Главная страница', 'app_label': 'Главная страница', 'app_url': '/admin/', 'has_module_perms': True,
     'models': [
        {'name': 'Фоновое видео', 'object_name': 'BackgroundVideo',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/landing/backgroundvideo/',
         'add_url': '/admin/landing/backgroundvideo/add/'
        },
        {'name': 'Предложение', 'object_name': 'Offer',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/landing/offer/',
         'add_url': '/admin/landing/offer/add/'
        },
        {'name': 'Showreel', 'object_name': 'Showreel',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/landing/showreel/',
         'add_url': '/admin/landing/showreel/add/'
        },
        {'name': 'Кейсы с видео', 'object_name': 'VideoCase',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/landing/videocase/',
         'add_url': '/admin/landing/videocase/add/'
        },
        {'name': 'Клиенты', 'object_name': 'Clients',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/landing/clients/',
         'add_url': '/admin/landing/clients/add/'
        },
        {'name': 'Команда', 'object_name': 'Team',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/landing/team/',
         'add_url': '/admin/landing/team/add/'
        },
     ]},

    {'name': 'Страница с портфолио', 'app_label': 'Страница с портфолио', 'app_url': '/admin/', 'has_module_perms': True,
     'models': [
        {'name': 'Типы видео', 'object_name': 'TypeVideo',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/landing/typevideo/',
         'add_url': '/admin/landing/typevideo/add/'
        },
        {'name': 'Портфолио', 'object_name': 'VideoPortfolio',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/landing/videoportfolio/',
         'add_url': '/admin/landing/videoportfolio/add/'
        },
     ]},

    {'name': 'Квиз', 'app_label': 'Квиз', 'app_url': '/admin/', 'has_module_perms': True,
     'models': [
        {'name': 'Вопросы', 'object_name': 'Question',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/landing/question/',
         'add_url': '/admin/landing/question/add/'
        },
     ]},

     {'name': 'Настройки', 'app_label': 'Настройки', 'app_url': '/admin/', 'has_module_perms': True,
     'models': [
        {'name': 'Почта для отправки уведомлений', 'object_name': 'DynamicEmailConfiguration',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/des/dynamicemailconfiguration/',
        },
        {'name': 'Простые блоки', 'object_name': 'FlatBlock',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/flatblocks/flatblock/',
        },
        {'name': 'Иное', 'object_name': 'Config',
         'perms': {'add': True, 'change': True, 'delete': True},
         'admin_url': '/admin/constance/config/',
        },
    ]},

    {'name': 'Пользователи', 'app_label': 'Пользователи', 'app_url': '/admin/', 'has_module_perms': True,
    'models': [
       {'name': 'Пользователи', 'object_name': 'User',
        'perms': {'add': True, 'change': True, 'delete': True},
        'admin_url': '/admin/auth/user/',
        'add_url': '/admin/auth/user/add/'
       },
       {'name': 'Группы пользователей', 'object_name': 'Group',
        'perms': {'add': True, 'change': True, 'delete': True},
        'admin_url': '/admin/auth/group/',
        'add_url': '/admin/auth/group/add/'
       },
    ]},
]
