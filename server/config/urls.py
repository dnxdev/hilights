from django.urls import include, path
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from django.views.generic import TemplateView
from django.contrib.sitemaps.views import sitemap

from des import urls as des_urls

from apps.my_sitemaps.sitemaps import MySitemap


sitemaps = {
    'static': MySitemap,
}

urlpatterns = [
    path('', include('apps.landing.urls')),
    path('admin/', admin.site.urls),

    path('robots.txt', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),

    path('django-des/', include(des_urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

    import debug_toolbar

    urlpatterns += [
          path('__debug__/', include(debug_toolbar.urls)),
    ]